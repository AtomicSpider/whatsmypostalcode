package com.satan.wtfismypostalcode.activities;

/**
 * Project : wtfismypostalcode
 * Created by Sanat Dutta on 2/12/2015.
 */

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.satan.wtfismypostalcode.R;

public class DonateActivity extends AppCompatActivity {

    String TAG = "DonateActivity";

    //Views
    private Spinner mSpinner;
    private TextView mDonateButton;

    //Data
    private int donateOption = 0;
    private String redirectUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate);

        Log.i(TAG, "DonateActivity: onCreate");

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setTitle("");
        }

        findViews();
        setUpSpinner();
        setUpSpinnerListener();
        setDonateButtonClickListener();
    }

    private void findViews() {
        mSpinner = (Spinner) findViewById(R.id.donateAmountSpinner);
        mDonateButton = (TextView) findViewById(R.id.donateButton);
    }

    private void setUpSpinner() {
        ArrayAdapter<CharSequence> mAdapter = ArrayAdapter.createFromResource(this,
                R.array.donation_amount, android.R.layout.simple_spinner_item);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(mAdapter);
    }

    private void setUpSpinnerListener() {
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                donateOption = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setDonateButtonClickListener() {
        mDonateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeRedirectUrl();
                goToPayPalDonatePage();
            }
        });
    }

    private void makeRedirectUrl() {
        String URL_B;
        switch (donateOption) {
            case 0:
                URL_B = "0.5";
                break;
            case 1:
                URL_B = "1";
                break;
            case 2:
                URL_B = "2";
                break;
            case 3:
                URL_B = "3";
                break;
            case 4:
                URL_B = "5";
                break;
            case 5:
                URL_B = "10";
                break;
            case 6:
                URL_B = "20";
                break;
            case 7:
                URL_B = "30";
                break;
            case 8:
                URL_B = "50";
                break;
            case 9:
                URL_B = "100";
                break;
            default:
                URL_B = "0.5";
        }

        Log.i(TAG, "Donate Amount: $ " + URL_B);

        String URL_A = "https://www.paypal.com/cgi-bin/webscr?business=sonu4414@gmail.com&cmd=_xclick&currency_code=USD&amount=";
        String URL_C = "&item_name=Donate";

        redirectUrl = URL_A + URL_B + URL_C;
    }

    private void goToPayPalDonatePage() {
        Intent mIntent = new Intent(Intent.ACTION_VIEW);
        mIntent.setData(Uri.parse(redirectUrl));
        startActivity(mIntent);
        finish();
    }

    /*********************************** Default Methods ******************************************/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_donate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.info:
                showInfoDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showInfoDialog() {
        new MaterialDialog.Builder(this)
                .content("You will be directed to a PayPal page where you can donate with your Debit Card, Credit Card or Paypal Account.")
                .positiveText("Okay")
                .positiveColorRes(R.color.colorPrimaryDark)
                .theme(Theme.LIGHT)
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
