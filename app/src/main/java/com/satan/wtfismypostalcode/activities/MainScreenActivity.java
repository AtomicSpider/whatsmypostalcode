package com.satan.wtfismypostalcode.activities;

/**
 * Project : wtfismypostalcode
 * Created by Sanat Dutta on 2/17/2015.
 */

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.satan.wtfismypostalcode.R;
import com.satan.wtfismypostalcode.common.RateTheApp;
import com.satan.wtfismypostalcode.common.Utils;
import com.satan.wtfismypostalcode.models.PlaceObject;
import com.satan.wtfismypostalcode.models.PredictionObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MainScreenActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {

    private String TAG = "MainScreenActivity";

    //Views
    FrameLayout markerButton, addressButton;
    private RelativeLayout markerSelected, markerUnselected, addressSelected, addressUnselected;
    private ImageView menu, autoButton;
    private CircularProgressView mProgressBar;
    private TextView postalCodeText, locationText;
    private AutoCompleteTextView mAutoCompleteTextView;

    //Interfaces
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mGoogleMap;
    private ArrayAdapter<String> addressAdapter;

    //Data
    private int maxResults = 20;
    private int currentTab = 0;

    private String postalCodeTextData = null, locationTextData = null;
    private double mLatitude = 37.422360, mLongitude = -122.084225;
    private ArrayList<String> permissions_code, manifestPermissions;
    private String browserApiKey;
    private ArrayList<PredictionObject> predictionsArray;

    private boolean firstOnConnected = true;
    private boolean postalReplace = false, addressReplace = false;
    private boolean isProcessOngoing = false;
    private boolean isAppReadyToLaunch = false;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private final static int ACCESS_FINE_LOCATION_REQUEST_CODE = 1001;
    private final static int ACCESS_COARSE_LOCATION_REQUEST_CODE = 1002;
    private final static int ACCESS_NETWORK_STATE_REQUEST_CODE = 1003;
    private final static int INTERNET_REQUEST_CODE = 1004;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "MainScreenActivity: onCreate()");

        constructStrings();
        launchApp();
    }

    private void constructStrings() {
        browserApiKey = getResources().getString(R.string.browserApiKey);
        permissions_code = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.permissions)));
        manifestPermissions = new ArrayList<>();
        manifestPermissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        manifestPermissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        manifestPermissions.add(Manifest.permission.ACCESS_NETWORK_STATE);
        manifestPermissions.add(Manifest.permission.INTERNET);
    }

    private void launchApp() {
        if (isAppReadyToLaunch) {
            Log.i(TAG, "App ready to launch");
            if (checkGooglePlayServices()) {
                setContentView(R.layout.activity_main_screen);
                findViews();
                setUpCustomTabs();
                setupContextMenu();
                setAutoLocateListener();
                setClipBoardListener();
                initializeGoogleMap();
            }
        } else {
            Log.i(TAG, "App does not have all permissions");
            getPermissions();
        }
    }

    private void getPermissions() {
        for (int i = 0; i < permissions_code.size(); i++) {
            if (ContextCompat.checkSelfPermission(MainScreenActivity.this, manifestPermissions.get(i)) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainScreenActivity.this, new String[]{manifestPermissions.get(i)}, Integer.parseInt(permissions_code.get(i)));

                Log.i(TAG, "App permission not present. Code: " + permissions_code.get(i));
                isAppReadyToLaunch = false;
                break;
            }
            Log.i(TAG, "App permission present. Code: " + permissions_code.get(i));
            isAppReadyToLaunch = true;
            if (i == permissions_code.size() - 1) {
                Log.i(TAG, "All permissions granted. Launching App");
                launchApp();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (requestCode == ACCESS_FINE_LOCATION_REQUEST_CODE || requestCode == ACCESS_COARSE_LOCATION_REQUEST_CODE || requestCode == ACCESS_NETWORK_STATE_REQUEST_CODE || requestCode == INTERNET_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPermissions();
            } else {
                Log.e(TAG, "User did not grant permission");
                Toast.makeText(MainScreenActivity.this, "Permission required for app functionality", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    private void findViews() {
        mProgressBar = (CircularProgressView) findViewById(R.id.fetching_data_progressBar);
        postalCodeText = (TextView) findViewById(R.id.postal_code_text_view);
        locationText = (TextView) findViewById(R.id.location_text_view);
        autoButton = (ImageView) findViewById(R.id.auto_button);
        mAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.auto_complete_textview);
    }

    private void setUpCustomTabs() {
        markerButton = (FrameLayout) findViewById(R.id.marker_tab_button);
        addressButton = (FrameLayout) findViewById(R.id.address_tab_button);
        markerSelected = (RelativeLayout) findViewById(R.id.marker_tab_selected);
        markerUnselected = (RelativeLayout) findViewById(R.id.marker_tab_unselected);
        addressSelected = (RelativeLayout) findViewById(R.id.address_tab_selected);
        addressUnselected = (RelativeLayout) findViewById(R.id.address_tab_unselected);

        markerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentTab != 0) {
                    if (!isProcessOngoing) {
                        locationText.setText("Long tap on map or tap auto locate button");
                        postalCodeText.setText("ZIP CODE");
                        currentTab = 0;
                        mGoogleMap.clear();
                        autoButton.setVisibility(View.VISIBLE);
                        markerSelected.setVisibility(View.VISIBLE);
                        markerUnselected.setVisibility(View.GONE);
                        addressSelected.setVisibility(View.GONE);
                        addressUnselected.setVisibility(View.VISIBLE);
                        mAutoCompleteTextView.setText("");
                        mAutoCompleteTextView.setVisibility(View.GONE);
                        locationText.setVisibility(View.VISIBLE);
                    } else searchRunning();
                }
            }
        });

        addressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentTab != 1) {
                    if (!isProcessOngoing) {
                        postalCodeText.setText("ZIP CODE");
                        currentTab = 1;
                        mGoogleMap.clear();
                        autoButton.setVisibility(View.GONE);
                        markerSelected.setVisibility(View.GONE);
                        markerUnselected.setVisibility(View.VISIBLE);
                        addressSelected.setVisibility(View.VISIBLE);
                        addressUnselected.setVisibility(View.GONE);
                        mAutoCompleteTextView.setVisibility(View.VISIBLE);
                        locationText.setVisibility(View.GONE);
                    } else searchRunning();
                }
            }
        });
    }

    private void setupContextMenu() {
        menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(MainScreenActivity.this, menu);
                popup.getMenuInflater().inflate(R.menu.menu_main, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.donate:
                                Intent goToDonateActivity = new Intent(MainScreenActivity.this, DonateActivity.class);
                                startActivity(goToDonateActivity);
                                break;
                            case R.id.rate:
                                Intent rateIntent = new Intent(Intent.ACTION_VIEW);
                                rateIntent.setData(Uri.parse("market://details?id=com.satan.wtfismypostalcode"));
                                startActivity(rateIntent);
                                break;
                            case R.id.contact:
                                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                                emailIntent.setData(Uri.parse("mailto:sonu4414@gmail.com"));
                                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "What's My Postal Code App");
                                startActivity(Intent.createChooser(emailIntent, "Email with..."));
                                break;
                            case R.id.help:
                                showHelpDialog();
                                break;
                        }
                        return true;
                    }
                });

                popup.show();
            }
        });
    }

    private void setAutoLocateListener() {
        autoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isProcessOngoing)
                    if (Utils.isConnectedToInternet(MainScreenActivity.this)) getGeoLocation();
                    else toastNetworkOn();
                else searchRunning();
            }
        });
    }

    private void setClipBoardListener() {
        locationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringExtracted = locationText.getText().toString();
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager mClipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    mClipboard.setText(stringExtracted);
                } else {
                    android.content.ClipboardManager mClipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData mClipData = android.content.ClipData.newPlainText("Copied Text", stringExtracted);
                    mClipboard.setPrimaryClip(mClipData);
                }
                Toast.makeText(MainScreenActivity.this, "Location Copied to Clipboard", Toast.LENGTH_SHORT).show();
            }
        });
        postalCodeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringExtracted = postalCodeText.getText().toString();
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager mClipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    mClipboard.setText(stringExtracted);
                } else {
                    android.content.ClipboardManager mClipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData mClipData = android.content.ClipData.newPlainText("Copied Text", stringExtracted);
                    mClipboard.setPrimaryClip(mClipData);
                }
                Toast.makeText(MainScreenActivity.this, "Postal Code Copied to Clipboard", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean checkGooglePlayServices() {

        GoogleApiAvailability mGoogleApiAvailability = GoogleApiAvailability.getInstance();
        int mResultCode = mGoogleApiAvailability.isGooglePlayServicesAvailable(MainScreenActivity.this);
        if (mResultCode != ConnectionResult.SUCCESS) {

            Log.e(TAG, "Google Play Services Not Installed");
            Toast.makeText(MainScreenActivity.this, "Google Play Services Not Installed", Toast.LENGTH_SHORT).show();

            if (mGoogleApiAvailability.isUserResolvableError(mResultCode)) {
                mGoogleApiAvailability.getErrorDialog(this, mResultCode,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }

            finish();
            return false;
        }
        return true;
    }

    private void initializeGoogleMap() {
        try {
            LoadGoogleMap();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Could not load Google Maps");
            Toast.makeText(this, "Could not load Google Maps", Toast.LENGTH_SHORT).show();
        }
    }

    private void LoadGoogleMap() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i(TAG, "Google maps loaded.");
        mGoogleMap = googleMap;
        connectToGoogleApi();
        setGoogleMapOption();
        setMapOnClickListener();
        setUpAutoCompleteLocation();
        setAppRate();
    }

    private void connectToGoogleApi() {
        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "onConnected");
        if (firstOnConnected) {
            if (Utils.isConnectedToInternet(MainScreenActivity.this)) getGeoLocation();
            else toastNetworkOn();

            firstOnConnected = false;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    // AutoLocation Methods ***************************************************************

    private void getGeoLocation() {

        locationText.setText("Long tap on map or tap auto locate button");
        postalCodeText.setText("ZIP CODE");

        isProcessOngoing = true;
        mProgressBar.setVisibility(View.VISIBLE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            ;

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            mLatitude = mLastLocation.getLatitude();
            mLongitude = mLastLocation.getLongitude();
            Log.i(TAG, "Latitude GPS: " + mLatitude + " Longitude GPS: " + mLongitude);
            displayMyLocation();
        } else {
            Log.e(TAG, "Could not get location vis GPS");
            Toast.makeText(MainScreenActivity.this, "Couldn't get location. Please make sure that GPS is enabled on your device.", Toast.LENGTH_LONG).show();
            isProcessOngoing = false;
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void displayMyLocation() {
        addLocationMarker();
        moveCameraToMyLocation();
        (new GetAddress(this)).execute(mLatitude, mLongitude);
    }

    private void setGoogleMapOption() {
        //Show current Location
        //mGoogleMap.setMyLocationEnabled(true);
        //Show Zooming Buttons
        //mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        //Enable zoom by pinch
        mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);
        //Enable Compass functionality
        mGoogleMap.getUiSettings().setCompassEnabled(true);
        //Enable my location button
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
        //Enable rotating gesture
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(true);
    }

    private void addLocationMarker() {
        mGoogleMap.clear();
        MarkerOptions mMarker = new MarkerOptions().position(new LatLng(mLatitude, mLongitude)).title("Location");
        mGoogleMap.addMarker(mMarker);
    }

    private void moveCameraToMyLocation() {
        CameraPosition mCameraPosition = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(mGoogleMap.getMaxZoomLevel() - 7).build();
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
    }

    private class GetAddress extends AsyncTask<Double, Void, ArrayList<String>> {

        Context mContext;
        double latitude;
        double longitude;

        public GetAddress(Context aContext) {
            super();
            mContext = aContext;
        }

        @Override
        protected ArrayList<String> doInBackground(Double... params) {

            String mAddress = "";
            String mPostalCode = "";
            ArrayList<String> mResult = new ArrayList<>();
            mResult.add(0, mPostalCode);
            mResult.add(1, mAddress);

            Geocoder mGeocoder = new Geocoder(mContext, Locale.getDefault());
            latitude = params[0];
            longitude = params[1];

            List<Address> addressResults;

            try {
                if (Geocoder.isPresent()) {
                    Log.i(TAG, "Geocoder Present");
                    addressResults = mGeocoder.getFromLocation(latitude, longitude, maxResults);

                    for (Address mLocation : addressResults) {
                        if (mLocation != null) {
                            Log.i(TAG, "Location from Geocoder: " + mLocation);
                            if (mLocation.getPostalCode() != null && mPostalCode.equals(""))
                                mPostalCode = mLocation.getPostalCode();
                            if ((mLocation.getAddressLine(0) != null || mLocation.getAddressLine(1) != null) && mAddress.equals("")) {
                                if (mLocation.getAddressLine(0) != null)
                                    mAddress = mLocation.getAddressLine(0) + ", ";
                                if (mLocation.getAddressLine(1) != null)
                                    mAddress += mLocation.getAddressLine(1) + ", ";
                                if (mLocation.getLocality() != null)
                                    mAddress += ", " + mLocation.getLocality();
                            }
                        }
                        if (!mPostalCode.equals("") && !mAddress.equals("")) break;
                    }

                    mResult.add(0, mPostalCode);
                    mResult.add(1, mAddress);
                    Log.i(TAG, "Geocoder : PostalCode-" + mPostalCode + " Address-" + mAddress);

                } else Log.e(TAG, "No Geocoder, getting location via web json");
            } catch (IOException e) {
                Log.e(TAG, "I/O Failure", e);
            }
            return mResult;
        }

        @Override
        protected void onPostExecute(ArrayList<String> mResult) {

            String mPostalCode = mResult.get(0);
            String mAddress = mResult.get(1);

            if (mPostalCode.equals("") || mAddress.equals("")) {
                Log.i(TAG, "One or more null Location");

                if (mPostalCode.equals("")) {
                    postalReplace = true;
                    Log.i(TAG, "postalReplace: " + postalReplace);
                } else {
                    postalCodeTextData = mPostalCode;
                    postalReplace = false;
                }

                if (mAddress.equals("")) {
                    addressReplace = true;
                    Log.i(TAG, "addressReplace: " + addressReplace);
                } else {
                    locationTextData = mAddress;
                    addressReplace = false;
                }

                new getLocationInfoViaWeb().execute(latitude, longitude);
            } else {
                locationTextData = mAddress.trim();
                postalCodeTextData = mPostalCode.trim();

                viewLocationData();
            }
        }
    }

    private void viewLocationData() {
        isProcessOngoing = false;
        mProgressBar.setVisibility(View.GONE);
        locationText.setText(locationTextData);
        if (postalCodeTextData != null)
            postalCodeText.setText(postalCodeTextData);
        else
            Toast.makeText(MainScreenActivity.this, "Couldn't locate ZIP CODE, It seems Postal Code of this location is not yet available in Google Database", Toast.LENGTH_LONG).show();
    }

    private class getLocationInfoViaWeb extends AsyncTask<Double, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Double... params) {

            Log.i(TAG, "getLocationInfoViaWeb");

            double lat = params[0];
            double lng = params[1];

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            JSONObject jsonObject = null;

            try {
                String jsonString = null;
                URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=" + browserApiKey);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuilder buffer = new StringBuilder();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line).append("\n");
                }

                if (buffer.length() == 0) {
                    return null;
                }
                jsonString = buffer.toString();
                jsonObject = new JSONObject(jsonString);
            } catch (IOException | JSONException e) {
                Log.e("getLocationInfoViaWeb", "Error ", e);
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        Log.e("getLocationInfoViaWeb", "Error closing stream", e);
                    }
                }
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            PlaceObject mPlaceObject = new PlaceObject();

            try {
                String status = jsonObject.getString("status");
                if (status.equalsIgnoreCase("OK")) {

                    JSONArray resultArray = jsonObject.getJSONArray("result");

                    for (int j = 0; j < resultArray.length(); j++) {
                        JSONObject resultObject = resultArray.getJSONObject(j);
                        JSONArray addressComponentsArray = resultObject.getJSONArray("address_components");
                        for (int i = 0; i < addressComponentsArray.length(); i++) {
                            JSONObject mAddressJsonObject = addressComponentsArray.getJSONObject(i);
                            JSONArray typesArray = mAddressJsonObject.getJSONArray("types");

                            if (typesArray.getString(0).equals("postal_code")) {
                                Log.i(TAG, "Postal Code found in Place detail");
                                mPlaceObject.setPostalCode(mAddressJsonObject.getString("long_name"));
                                mPlaceObject.setError(false);
                                break;
                            }
                        }

                        mPlaceObject.setAddress(resultObject.getString("formatted_address"));
                        Log.i(TAG, "Address: " + mPlaceObject.getAddress());
                        mPlaceObject.setError(false);
                        if (mPlaceObject.getPostalCode() != null) break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (addressReplace) locationTextData = "Location Not Found";
            if (postalReplace) postalCodeTextData = null;

            if (!mPlaceObject.isError()) {

                if ((mPlaceObject.getPostalCode() != null) && postalReplace)
                    postalCodeTextData = mPlaceObject.getPostalCode();
                if ((mPlaceObject.getAddress() != null) && addressReplace)
                    locationTextData = mPlaceObject.getAddress();
            }

            Log.i(TAG, "Null Location Corrected: PostalCode-" + postalCodeTextData + " Address-" + locationTextData);

            viewLocationData();
        }
    }

    // MarkerLocation Methods ***************************************************************

    private void setMapOnClickListener() {
        mGoogleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                if (!isProcessOngoing) {
                    if (Utils.isConnectedToInternet(MainScreenActivity.this)) {
                        if (currentTab == 0) {
                            locationText.setText("Long tap on map or tap auto locate button");
                            postalCodeText.setText("ZIP CODE");

                            isProcessOngoing = true;
                            mProgressBar.setVisibility(View.VISIBLE);

                            mLatitude = latLng.latitude;
                            mLongitude = latLng.longitude;
                            Log.i(TAG, "Latitude Mark: " + mLatitude + " Longitude Mark: " + mLongitude);
                            addLocationMarker();
                            moveCameraToMyLocation();
                            (new GetAddress(MainScreenActivity.this)).execute(mLatitude, mLongitude);
                        }
                    } else toastNetworkOn();
                } else searchRunning();
            }
        });
    }

    // AddressLocation Methods ***************************************************************

    private void setUpAutoCompleteLocation() {
        addressAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.item_auto_complete_textview);
        addressAdapter.setNotifyOnChange(true);

        mAutoCompleteTextView.setAdapter(addressAdapter);
        mAutoCompleteTextView.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count % 3 == 1) {
                    addressAdapter.clear();
                    if (Utils.isConnectedToInternet(MainScreenActivity.this)) {
                        if (!isProcessOngoing) (new GetPlaces()).execute(s.toString());
                    } else toastNetworkOn();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "AutoCompleteTextView Position: " + position);

                postalCodeText.setText("ZIP CODE");

                String chosenLocation = mAutoCompleteTextView.getText().toString();
                if (!chosenLocation.isEmpty()) {
                    Utils.hideKeyboard(MainScreenActivity.this, mAutoCompleteTextView.getWindowToken());

                    if (!isProcessOngoing) {
                        if (Utils.isConnectedToInternet(MainScreenActivity.this)) {
                            Log.i(TAG, "description: " + predictionsArray.get(position).getDescription() + " place_id: " + predictionsArray.get(position).getPlace_id());
                            (new GetPlaceDetails()).execute(predictionsArray.get(position).getPlace_id());
                        } else toastNetworkOn();
                    } else searchRunning();
                }
            }
        });
    }

    private class GetPlaces extends AsyncTask<String, Void, ArrayList<PredictionObject>> {

        String autoLocationString = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isProcessOngoing = true;
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<PredictionObject> doInBackground(String... args) {
            Log.i(TAG, "GetPlaces: doInBackground");
            autoLocationString = args[0];
            predictionsArray = new ArrayList<>();
            try {
                URL googlePlacesUrl = new URL("https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + URLEncoder.encode(autoLocationString, "UTF-8") + "&language=en&key=" + browserApiKey);
                URLConnection mURLConnection = googlePlacesUrl.openConnection();
                BufferedReader mBufferedReader = new BufferedReader(new InputStreamReader(mURLConnection.getInputStream()));

                String jsonOutputLine;
                StringBuilder mStringBuffer = new StringBuilder();

                while ((jsonOutputLine = mBufferedReader.readLine()) != null)
                    mStringBuffer.append(jsonOutputLine);

                JSONObject predictionsJSONObject = new JSONObject(mStringBuffer.toString());
                JSONArray mJSONArray = new JSONArray(predictionsJSONObject.getString("predictions"));

                for (int i = 0; i < mJSONArray.length(); i++) {
                    JSONObject mJSONObject = (JSONObject) mJSONArray.get(i);
                    predictionsArray.add(new PredictionObject(mJSONObject.getString("description"), mJSONObject.getString("place_id")));
                }
            } catch (IOException | JSONException e) {
                Log.e(TAG, "GetPlaces : doInBackground", e);
            }
            return predictionsArray;
        }

        @Override
        protected void onPostExecute(ArrayList<PredictionObject> predictionsArrayResult) {
            Log.i(TAG, "onPostExecute predictionsArray size : " + predictionsArrayResult.size());

            mProgressBar.setVisibility(View.GONE);
            isProcessOngoing = false;

            addressAdapter = new ArrayAdapter<>(getBaseContext(), R.layout.item_auto_complete_textview);
            addressAdapter.setNotifyOnChange(true);

            mAutoCompleteTextView.setAdapter(addressAdapter);

            for (PredictionObject mPredictionObject : predictionsArrayResult) {
                Log.i(TAG, "onPostExecute : predictionsArray results = " + mPredictionObject.getDescription());
                addressAdapter.add(mPredictionObject.getDescription());
                addressAdapter.notifyDataSetChanged();
            }
        }
    }

    class GetPlaceDetails extends AsyncTask<String, Void, PlaceObject> {

        String place_id = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isProcessOngoing = true;
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected PlaceObject doInBackground(String... args) {
            Log.i(TAG, "GetPlaceDetails: doInBackground");
            place_id = args[0];
            PlaceObject mPlaceObject = new PlaceObject();

            try {
                URL googlePlacesUrl = new URL("https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key=" + browserApiKey);
                URLConnection mURLConnection = googlePlacesUrl.openConnection();
                BufferedReader mBufferedReader = new BufferedReader(new InputStreamReader(mURLConnection.getInputStream()));

                String jsonOutputLine = "";
                StringBuilder mStringBuffer = new StringBuilder();

                while ((jsonOutputLine = mBufferedReader.readLine()) != null)
                    mStringBuffer.append(jsonOutputLine);

                JSONObject mJSONObject = new JSONObject(mStringBuffer.toString());

                String status = mJSONObject.getString("status");
                if (status.equalsIgnoreCase("OK")) {
                    JSONObject resultObject = mJSONObject.getJSONObject("result");
                    JSONArray addressComponentsArray = resultObject.getJSONArray("address_components");
                    for (int i = 0; i < addressComponentsArray.length(); i++) {
                        JSONObject mAddressJsonObject = addressComponentsArray.getJSONObject(i);
                        JSONArray typesArray = mAddressJsonObject.getJSONArray("types");

                        if (typesArray.getString(0).equals("postal_code")) {
                            Log.i(TAG, "Postal Code found in Place detail");
                            mPlaceObject.setPostalCode(mAddressJsonObject.getString("long_name"));
                            mPlaceObject.setError(false);
                            break;
                        }
                    }
                    JSONObject locationObject = resultObject.getJSONObject("geometry").getJSONObject("location");
                    mPlaceObject.setLatitude(Double.parseDouble(locationObject.getString("lat")));
                    mPlaceObject.setLongitude(Double.parseDouble(locationObject.getString("lng")));
                    mPlaceObject.setError(false);

                    Log.i(TAG, "GeoPoints. Latitude: " + mPlaceObject.getLatitude() + " Longitude: " + mPlaceObject.getLongitude());
                }
            } catch (IOException | JSONException e) {
                Log.e(TAG, "GetPlaces : doInBackground", e);
            }
            return mPlaceObject;
        }

        @Override
        protected void onPostExecute(PlaceObject mPlaceObject) {

            Log.i(TAG, "onPostExecute: GetPlaceDetails");

            if (!mPlaceObject.isError()) {

                mLatitude = mPlaceObject.getLatitude();
                mLongitude = mPlaceObject.getLongitude();

                if (mPlaceObject.getPostalCode() != null) {
                    Log.i(TAG, "PlaceDetail Postal Code: " + mPlaceObject.getPostalCode());

                    isProcessOngoing = false;
                    mProgressBar.setVisibility(View.GONE);
                    postalCodeText.setText(mPlaceObject.getPostalCode());
                    addLocationMarker();
                    moveCameraToMyLocation();
                } else {
                    displayMyLocation();
                }
            } else {
                isProcessOngoing = false;
                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(MainScreenActivity.this, "Couldn't locate ZIP CODE, It seems Postal Code of this location is not yet available in Google Database", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setAppRate() {
        RateTheApp.onStart(this);
        RateTheApp.showRateDialogIfNeeded(this);
    }

    // Extra Methods ***************************************************************

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void toastNetworkOn() {
        Toast.makeText(MainScreenActivity.this, "Please switch on the network", Toast.LENGTH_SHORT).show();
    }

    private void searchRunning() {
        Toast.makeText(MainScreenActivity.this, "Search running...", Toast.LENGTH_SHORT).show();
    }

    private void showHelpDialog() {

        MaterialDialog mMaterialDialog = new MaterialDialog.Builder(this)
                .title("Help")
                .titleColorRes(R.color.teal_700)
                .customView(R.layout.dialog_about, false)
                .positiveText("Okay")
                .positiveColorRes(R.color.teal_700)
                .theme(Theme.LIGHT)
                .build();

        TextView devText = (TextView) mMaterialDialog.findViewById(R.id.dev);
        String mData = "<html><body>" + "<p>App developed by " + "<a href='" + "http://www.satandigital.com/" + "'>" + "Sanat Dutta" + "</a>" + "</body></html>";
        devText.setText(Html.fromHtml(mData));
        devText.setMovementMethod(LinkMovementMethod.getInstance());
        devText.setLinkTextColor(getResources().getColor(R.color.link_color));
        devText.setTextSize(16);

        mMaterialDialog.show();
    }
}