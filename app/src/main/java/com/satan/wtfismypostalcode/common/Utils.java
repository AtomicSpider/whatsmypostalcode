package com.satan.wtfismypostalcode.common;

/**
 * Project : wtfismypostalcode
 * Created by Sanat Dutta on 2/18/2015.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.view.inputmethod.InputMethodManager;

public class Utils {
    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting());
    }

    public static void hideKeyboard(Context c, IBinder windowToken) {
        InputMethodManager mInputMethodManager = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        mInputMethodManager.hideSoftInputFromWindow(windowToken, 0);
    }
}
