package com.satan.wtfismypostalcode.models;

/**
 * Project : wtfismypostalcode
 * Created by Sanat Dutta on 4/26/2016.
 */

public class PlaceObject {

    boolean error = true;
    double latitude=0;
    double longitude=0;
    String postalCode = null;
    String address = null;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
