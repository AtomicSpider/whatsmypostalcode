package com.satan.wtfismypostalcode.models;

/**
 * Project : wtfismypostalcode
 * Created by Sanat Dutta on 4/26/2016.
 */

public class PredictionObject {

    String description;
    String place_id;

    public PredictionObject(String description, String place_id) {
        this.description = description;
        this.place_id = place_id;
    }

    public String getDescription() {
        return description;
    }

    public String getPlace_id() {
        return place_id;
    }
}
